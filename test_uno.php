<?php
declare(ticks=1);

interface Pajaro
{
    public function ponerHuevo() : Huevo;
}

interface Mamifero
{
    public function parir() : Cachorro;
}

class Pollo implements Pajaro
{
	public function ponerHuevo() : Huevo
	{
		return new Huevo(__CLASS__);
	}
}

class Perro implements Mamifero
{
    public function parir() : Cachorro
    {
        return new Cachorro(__CLASS__);
    }
}

class Huevo
{
	public $tipopajaro;
	public function __construct(string $tipopajaro)
    {
    	$this->tipopajaro = $tipopajaro . ' pone huevos ';
    }

    public function cascara()
    {
        echo $this->tipopajaro . 'con ' . __FUNCTION__;
    }

    public function fritos()
    {
        echo $this->tipopajaro . __FUNCTION__;
    }

    public function duros()
    {
        echo $this->tipopajaro . __FUNCTION__;
    }
}

class Cachorro
{
    public $tipomamifero;
    public function __construct(string $tipomamifero)
    {
        $this->tipomamifero = $tipomamifero . ' tiene crias ';
    }

    public function vivas()
    {
        echo $this->tipomamifero . __FUNCTION__;
    }

    public function danzarinas()
    {
        echo $this->tipomamifero . __FUNCTION__;
    }
}

/*
*
* A partir de las siguientes clases y sus interfaces, imrimir en pantalla
* 'Pollo pone huevos con cascara'
* 'Perro tiene crias danzarinas'
*
*/

$pollo = new Pollo();
$pollo = $pollo->ponerHuevo()->cascara();

echo $pollo . '<br>';

$perro = new Perro();
$perro = $perro->parir()->danzarinas();
echo $perro;





