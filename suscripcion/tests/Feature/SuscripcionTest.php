<?php

namespace Tests\Feature;

use App\Suscripcion;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SuscripcionTest extends TestCase
{
    use DatabaseTransactions;


    public function testSuscribirse(){

        $servicio_cliente = Suscripcion::where('activo', 0)->first();

        $data = array(
            "suscripcion" => array(
                "cliente" => $servicio_cliente->cliente_id,
                "servicio" => $servicio_cliente->servicio_id
            )
        );


        $response = $this->postJson('/api/suscripcion/suscribir', $data);
        $response->assertStatus(200)->assertJson([
            'status' => "ok",
            "message" => "Suscripción realizada correctamente",
        ]);
    }

    public function testCancelarSuscripcion(){
        $servicio_cliente = Suscripcion::where('activo', 1)->first();

        $data = array(
            "suscripcion" => array(
                "cliente" => $servicio_cliente->cliente_id,
                "servicio" => $servicio_cliente->servicio_id
            )
        );


        $response = $this->postJson('/api/suscripcion/cancelar', $data);
        $response->assertStatus(200)->assertJson([
            'status' => "ok",
            "message" => "Cancelación realizada correctamente",
        ]);
    }

}
