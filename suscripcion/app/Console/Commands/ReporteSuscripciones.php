<?php

namespace App\Console\Commands;

use App\Suscripcion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class ReporteSuscripciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reporteSuscripciones {fecha}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha = $this->argument('fecha');

        try {
            Carbon::parse($fecha);
        } catch (\Exception $e) {
            die("Ingrese una fecha con el formato aaaammdd o aaaa-mm-dd");
        }

        $suscripciones_totales = Suscripcion::where('activo', 1)->get();

        $suscripciones_hoy = Suscripcion::where('activo', 1)->where('fecha',$fecha)->get();
        $suscripciones_canceladas = Suscripcion::where('activo', 0)->where('fecha',$fecha)->get();

        echo 'Cantidad de suscripciones totales: '.count($suscripciones_totales)."\n";
        echo 'Cantidad de suscripciones: '.count($suscripciones_hoy)."\n";
        echo 'Cantidad de cancelaciones: '.count($suscripciones_canceladas)."\n";
    }
}
