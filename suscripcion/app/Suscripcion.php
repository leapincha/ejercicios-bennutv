<?php

namespace App;



class Suscripcion extends BaseModel
{
    protected $table = 'suscripciones';
    protected $fillable = ["fecha","activo"];

    protected $primaryKey = ['cliente_id', 'servicio_id'];
    public $incrementing = false;

    public function cliente(){
        return $this->hasOne("App\Cliente","id","cliente_id");
    }

    public function servicio(){
        return $this->hasOne("App\Models\User","id","servicio_id");
    }

    public function existe($query)
    {
        return $query->where('active', '=', 1)->where('');
    }
}
