<?php

namespace App\Http\Controllers;

use App\Suscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SuscripcionController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * Endpoint para suscribirse a un servicio
     */
    public function suscribirse(Request $request){
        if ($validator = $this->validacion($request->suscripcion)) {
            $errors = $validator->errors();
            $error = array(
                'status' => 'error',
                'error' => $errors
            );
            return $error;
        }

        $bodyContent = json_decode($request->getContent());
        $bodyContent = $bodyContent->suscripcion;

        $suscripcion = Suscripcion::where([
            ['cliente_id', '=', $bodyContent->cliente],
            ['servicio_id', '=', $bodyContent->servicio]])->first();

        if($suscripcion && $suscripcion->activo === 1){
            return response()->json(['status'=>'error','message'=>'El cliente ya se encuentra suscripto al servicio solicitado']);
        }

        $suscripcion = $this->constructObject($bodyContent, $suscripcion, 0);

        if($suscripcion->save()){
            return response()->json([
                'status'=>'ok',
                'status_code' => 200,
                'message'=> 'Suscripción realizada correctamente'
            ],200);
        }

        return response()->json(['status'=>'error','message'=>'Error al suscribir el servicio']);

    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * Endpoint para cancelar la suscripción del servicio
     */
    public function cancelar(Request $request){

        if ($validator = $this->validacion($request->suscripcion)) {
            $errors = $validator->errors();
            $error = array(
                'status' => 'error',
                'error' => $errors
            );
            return $error;
        }

        $bodyContent = json_decode($request->getContent());
        $bodyContent = $bodyContent->suscripcion;

        $suscripcion = Suscripcion::where([
            ['cliente_id', '=', $bodyContent->cliente],
            ['servicio_id', '=', $bodyContent->servicio]])->first();

        if($suscripcion && $suscripcion->activo === 0){
            return response()->json(['status'=>'error','message'=>'El cliente ya se encuentra dado de baja al servicio solicitado']);
        }

        $suscripcion = $this->constructObject($bodyContent, $suscripcion, 0);


        if($suscripcion->save()){
            return response()->json([
                'status'=>'ok',
                'status_code' => 200,
                'message'=> 'Cancelación realizada correctamente'
            ],200);
        }

        return response()->json(['status'=>'error','message'=>'Error al cancelar el servicio']);
    }

    /**
     * @param $data
     * @return bool
     * Método para validar los datos que se envían al servicio
     */
    private function validacion($data){
        $validator = Validator::make($data, [
            'cliente' => 'required',
            'servicio' => 'required'
        ]);

        return $validator->fails();
    }

    /**
     * @param $data
     * @param $suscripcion
     * @param $activo
     * @return Suscripcion
     * Método para construir el objeto y luego utilizarlo para guardar
     * o actualizar el registro
     */
    private function constructObject($data, $suscripcion, $activo){
        if(!$suscripcion){
            $suscripcion = new Suscripcion();
            $suscripcion->servicio_id = $data->servicio;
            $suscripcion->cliente_id = $data->cliente;
        }

        $suscripcion->fecha = date('Y-m-d');
        $suscripcion->activo = $activo;

        return $suscripcion;
    }
}
