<?php

use Illuminate\Database\Seeder;

class SuscripcionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $servicios = \App\Servicio::select('id')->get();
        $clientes = \App\Cliente::select('id')->get();

        foreach(range(1,500) as $index){

            $servicio = $faker->randomElement($servicios)->id;
            $cliente = $faker->randomElement($clientes)->id;

            $existe = \App\Suscripcion::where('cliente_id', $cliente)->where('servicio_id', $servicio)->first();

            if(!$existe){
                $suscripciones = \App\Suscripcion::create([
                    'cliente_id' => $cliente,
                    'servicio_id' => $servicio,
                    'fecha' => $faker->dateTimeThisMonth()->format('Y-m-d'),
                    'activo' => $faker->boolean,
                ]);
            }
        }

    }
}
