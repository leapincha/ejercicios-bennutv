<?php
/*
*
* Instanciar clase y metodo para imprimir '{Nombre de clase padre} - Hola mundo!'
*
*/

class ClaseExtendida
{
	public function hola()
	{
		return 'Hola mundo!';
	}
}

class D extends ClaseExtendida 
{
	public function f(ClaseExtendida $c) {
	    return get_class($c);
	}

	public static function imp()
	{
		return 'imprimir';
	}
}

class E{}

$class = new D();
$name_class = $class->f(new ClaseExtendida());
$hola = $class->hola();
echo $name_class. ' - '. $hola;

exit();