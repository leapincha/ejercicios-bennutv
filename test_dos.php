<?php

/**
*
* La cantidad de números pasados al método 'sumar' son siempre aleatorios. 
* Encontrar el error
*
*/

class variable
{
	public $resultado;

	public function sumar(array $numeros) : int
	{
		return array_sum($numeros);
	}
}

$sumamos = new variable();
$numeros = array(10,12,3,45,7,102);
echo $sumamos->sumar($numeros);
echo "<br>";

exit();