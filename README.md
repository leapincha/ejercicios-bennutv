# Ejercicios BennuTV

## Proyecto suscripciones

### Instalamos librerías de laravel
1. Dentro de la carpeta suscripcion corremos el comando: composer install
2. Copiamos el archivo .env.example a .env
3. Configuramos la conexión a la bd en el archivo .env 

### Base de datos
1. Crear base de datos suscripciones
2. Ejecutar migrations: php artisan migrate

### Ejecutar los seeders para llenar las tablas
1. Cargamos los servicios: php artisan db:seed --class=ServicioSeeder  
2. Para generar los clientes ingresamos el siguiente comando: php artisan tinker, ingresamos a la consola de tinker y ejecutamos lo siguiente: $clientes = factory(App\Cliente::class, 250)->create();    
3. Cargamos las suscripciones: php artisan db:seed --class=SuscripcionSeeder

### Levantamos el entorno
php artisan serve

Levanta el entorno en en el host http://localhost:8000

### Servicio para suscribirse a un servicio

- url: localhost:8000/api/suscripcion/suscribir
- method: POST

Ejemplo curl para suscribirse a un servicio:
>curl --location --request POST 'localhost:8000/api/suscripcion/suscribir' \
--header 'Content-Type: application/json' \
--data-raw '{
	"suscripcion": {
		"cliente": "1",
		"servicio": "3"
	}
}'
>

### Servicio para cancelar la suscripción

- url: localhost:8000/api/suscripcion/cancelar
- method: POST

Ejemplo curl para cancelar un servicio:
> curl --location --request POST 'localhost:8000/api/suscripcion/cancelar' \
--header 'Content-Type: application/json' \
--data-raw '{
	"suscripcion": {
		"cliente": "1",
		"servicio": "3"
	}
}'
>

### Script para obtener el reporte de suscripciones de la fecha deseada  
php artisan command:reporteSuscripciones 20200214


